import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;

public class Main {

    private static OkHttpClient client = new OkHttpClient();
    private static Request request;
    private static Response response;
    private static ObjectMapper objectMapper = new ObjectMapper();

    public static void main(String[] args) throws IOException {
        request = new Request.Builder().url("https://swapi.co/api/people/1/").build();
        response = client.newCall(request).execute();
        int code = response.code();
        String resp = response.body().string();
        System.out.println(code);
        System.out.println(resp);

        Person luke = objectMapper.readValue(resp,Person.class);

        System.out.println(luke.toString());



    }
}
